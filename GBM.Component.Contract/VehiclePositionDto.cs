﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GBM.Component.Contract
{
    /// <summary>
    /// Modelo que nos permitirá la comunicación entre componentes
    /// </summary>
    public class VehiclePositionDto
    {
        /// <summary>
        /// Id del registro de la relación del vehículo y su posición
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Id del vehiculo
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Latitud en la que se encuentra el vehículo
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Longitud en la que se encuentra el vehículo
        /// </summary>        
        public double Longitude { get; set; }
    }
}
