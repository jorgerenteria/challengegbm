﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GBM.Component.Contract
{
    public interface IVehiclePositionRepository
    {
        VehiclePositionDto Save(VehiclePositionDto vehiclePositionDto);
        VehiclePositionDto GetByVehicleId(int vehicleId);
    }
}
