﻿using AutoMapper;
using GBM.Component.Contract;
using MongoDB.Driver;

namespace GBM.Component.Implementation
{
    public class VehiclePositionRepository : IVehiclePositionRepository
    {
        private readonly IMongoCollection<VehiclePositionDb> collection;
        private readonly IMapper mapper;

        public VehiclePositionRepository(IMongoDatabase dataBase, IMapper mapper)
        {
            this.collection = dataBase.GetCollection<VehiclePositionDb>("vehiclePosition");
            this.mapper = mapper;
        }

        public VehiclePositionDto GetByVehicleId(int vehicleId)
        {
            var vehicleDb = this.collection.Find(vehicle => vehicle.VehicleId == vehicleId).SortByDescending(v => v.Id).FirstOrDefault();
            return vehicleDb != null ? this.mapper.Map<VehiclePositionDto>(vehicleDb) : null;
        }

        public VehiclePositionDto Save(VehiclePositionDto vehiclePositionDto)
        {
            var vehicleDb = new VehiclePositionDb
            {
                Latitude = vehiclePositionDto.Latitude,
                Longitude = vehiclePositionDto.Longitude,
                VehicleId = vehiclePositionDto.VehicleId
            };

            this.collection.InsertOne(vehicleDb);
            var vehicleDto = this.mapper.Map<VehiclePositionDto>(vehicleDb);
            return vehicleDto;
        }       
    }
}
