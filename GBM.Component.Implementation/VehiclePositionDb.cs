﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GBM.Component.Implementation
{
    public class VehiclePositionDb
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("VehicleId")]
        public int VehicleId { get; set; }

        [BsonElement("Latitude")]
        public double Latitude { get; set; }

        [BsonElement("Longitude")]
        public double Longitude { get; set; }
    }
}
