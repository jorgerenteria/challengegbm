using AutoMapper;
using GBM.Component.Contract;
using GMB.Challenge.Controllers;
using GMB.Challenge.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    public class Tests
    {
        [Test]        
        public void ShouldGetVehiclePositionWithStatusOk()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<VehiclePosition, VehiclePositionDto>();
                cfg.CreateMap<VehiclePositionDto, VehiclePosition>();
                
            });

            var mapper = new Mapper(config);

            var repositoryMock = new Mock<IVehiclePositionRepository>();
            repositoryMock.Setup(r => r.GetByVehicleId(It.IsAny<int>())).Returns(new VehiclePositionDto { Id = "1234567", VehicleId = 222, Latitude= 12.908765, Longitude= 15.123456 });
            var vehicleController = new VehicleController(repositoryMock.Object, mapper);            
            var result = vehicleController.GetVehiclePosition(222) as OkObjectResult;
            var vehiclePositionResult = result.Value as VehiclePosition;

            Assert.IsInstanceOf<VehiclePosition>(vehiclePositionResult);
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual("1234567", vehiclePositionResult.Id);
            Assert.AreEqual(222, vehiclePositionResult.VehicleId);
            Assert.AreEqual(12.908765, vehiclePositionResult.Latitude);
            Assert.AreEqual(15.123456, vehiclePositionResult.Longitude);
            Assert.AreEqual(200, result.StatusCode);
        }

        [Test]
        public void ShouldGetVehiclePositionWithStatus404()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<VehiclePosition, VehiclePositionDto>();
                cfg.CreateMap<VehiclePositionDto, VehiclePosition>();

            });

            var mapper = new Mapper(config);

            var repositoryMock = new Mock<IVehiclePositionRepository>();
            repositoryMock.Setup(r => r.GetByVehicleId(It.IsAny<int>())).Returns(default(VehiclePositionDto));
            var vehicleController = new VehicleController(repositoryMock.Object, mapper);
            var result = vehicleController.GetVehiclePosition(222) as NotFoundObjectResult;
            var vehiclePositionResult = result.Value as string;
            Assert.IsInstanceOf<string>(vehiclePositionResult);
            Assert.AreEqual("No se encontr� el veh�culo solicitado", vehiclePositionResult);
            Assert.AreEqual(404, result.StatusCode);
        }

        [Test]
        public void ShouldSaveVehiclePositionWithStatusCreated()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<VehiclePosition, VehiclePositionDto>();
                cfg.CreateMap<VehiclePositionDto, VehiclePosition>();
            });

            var mapper = new Mapper(config);

            var repositoryMock = new Mock<IVehiclePositionRepository>();
            repositoryMock.Setup(r => r.Save(It.IsAny<VehiclePositionDto>())).Returns(new VehiclePositionDto { Id = "1234567", VehicleId = 45, Latitude = 87.987654, Longitude = 65.986120 });
            var vehicleController = new VehicleController(repositoryMock.Object, mapper);
            var position = new Position { Latitude = 87.987654, Longitude = 65.986120 };
            var result = vehicleController.CreateVehiclePosition(45 ,position) as CreatedAtActionResult;
            var vehiclePositionResult = result.Value as VehiclePosition;
            
            Assert.IsInstanceOf<VehiclePosition>(vehiclePositionResult);
            Assert.AreEqual("1234567", vehiclePositionResult.Id);
            Assert.AreEqual(45, vehiclePositionResult.VehicleId);            
            Assert.AreEqual(87.987654, vehiclePositionResult.Latitude);
            Assert.AreEqual(65.986120, vehiclePositionResult.Longitude);
            Assert.AreEqual(201, result.StatusCode);            
        }

        [Test]
        public void ShouldSaveVehiclePositionWithStatusBadRequestPositionDataNull()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<VehiclePosition, VehiclePositionDto>();
                cfg.CreateMap<VehiclePositionDto, VehiclePosition>();
            });

            var mapper = new Mapper(config);

            var repositoryMock = new Mock<IVehiclePositionRepository>();
            repositoryMock.Setup(r => r.Save(It.IsAny<VehiclePositionDto>())).Returns(default(VehiclePositionDto));
            var vehicleController = new VehicleController(repositoryMock.Object, mapper);            
            var result = vehicleController.CreateVehiclePosition(45, default(Position)) as BadRequestObjectResult;
            var vehiclePositionResult = result.Value as string;

            Assert.IsInstanceOf<string>(vehiclePositionResult);
            Assert.AreEqual("Los datos solicitados son incorrectos", vehiclePositionResult);
            Assert.AreEqual(400, result.StatusCode);
        }

        [Test]
        public void ShouldSaveVehiclePositionWithStatusBadRequestIncompleteDataPosition()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<VehiclePosition, VehiclePositionDto>();
                cfg.CreateMap<VehiclePositionDto, VehiclePosition>();
            });

            var mapper = new Mapper(config);

            var repositoryMock = new Mock<IVehiclePositionRepository>();
            repositoryMock.Setup(r => r.Save(It.IsAny<VehiclePositionDto>())).Returns(default(VehiclePositionDto));
            var vehicleController = new VehicleController(repositoryMock.Object, mapper);
            var result = vehicleController.CreateVehiclePosition(45, new Position { Latitude = 22.652984 }) as BadRequestObjectResult;
            var vehiclePositionResult = result.Value as string;

            Assert.IsInstanceOf<string>(vehiclePositionResult);
            Assert.AreEqual("Los datos solicitados son incorrectos", vehiclePositionResult);
            Assert.AreEqual(400, result.StatusCode);
        }
    }
}