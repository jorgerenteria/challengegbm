﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GBM.Component.Contract;
using GMB.Challenge.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GMB.Challenge.Controllers
{
    [Produces("application/json")]
    [Route("api/vehicle")]
    public class VehicleController : Controller
    {

        private readonly IVehiclePositionRepository vehiclePositionRepository;
        private readonly IMapper mapper;

        public VehicleController(IVehiclePositionRepository vehiclePositionRepository, IMapper mapper)
        {
            this.vehiclePositionRepository = vehiclePositionRepository;
            this.mapper = mapper;
        }

      /// <summary>
      /// Recurso que nos permite obtener la última posición del vehículo
      /// </summary>      
      /// <param name="id">Id del vehiculo del que se quiere recuperar la posición</param>      
      /// <returns>VehiclePosition</returns>
      [HttpGet]
      [SwaggerResponse(200, "Obtiene la última posición del vehiculo", typeof(VehiclePosition))]
      [SwaggerResponse(404, "No se encontró el vehículo solicitado", typeof(string))]
      [Route("{id}/positions/last")]
      public IActionResult GetVehiclePosition(int id)
      {
          var vehicleDto = this.vehiclePositionRepository.GetByVehicleId(id);
         if(vehicleDto == null)
         {
            return NotFound("No se encontró el vehículo solicitado");
         }
          var vehicleModel = this.mapper.Map<VehiclePosition>(vehicleDto);
          return Ok(vehicleModel);
      }

      /// <summary>
      /// Recurso que nos permite almacenar la posición de un vehículo
      /// </summary>
      /// <remarks>
      /// Sample request:
      ///
      ///     POST /123/positions
      ///     {
      ///        "Latitude": 22.451263,
      ///        "Longitude": -15.159762    
      ///     }
      ///
      /// </remarks>
      /// <param name="id">Id del vehículo al que se le quiere registrar la posición</param>
      /// <param name="position">Objeto en el que se envia la longitud y lalitud del vehículo</param>
      /// <returns>VehiclePosition</returns>
      [HttpPost]
      [SwaggerResponse(201, "La posición del vehículo fue creada", typeof(VehiclePosition))]
      [SwaggerResponse(400, "Los datos solicitados son incorrectos", typeof(string))]      
      [Route("{id}/positions")]
      public IActionResult CreateVehiclePosition(int id, [FromBody]Position position)
      {
          if (position == null || position.Latitude == null || position.Longitude == null)
          {
              return BadRequest("Los datos solicitados son incorrectos");
          }


          var vehicleDto = new VehiclePositionDto
          {
              VehicleId = id,
              Latitude = position.Latitude.Value,
              Longitude = position.Longitude.Value
          };

          var vehiclePositionCreatedDto = this.vehiclePositionRepository.Save(vehicleDto);
          var vehiclePositionCreated = this.mapper.Map<VehiclePosition>(vehiclePositionCreatedDto);
          return CreatedAtAction(nameof(GetVehiclePosition), new { id = vehiclePositionCreated.Id }, vehiclePositionCreated);
      }
    }
}