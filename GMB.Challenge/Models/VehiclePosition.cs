﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMB.Challenge.Models
{
    /// <summary>
    /// Modelo que representa en que posición se encuentra el vehículo
    /// </summary>
    public class VehiclePosition
    {
        /// <summary>
        /// Id del registro de la relación del vehículo y su posición
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// Id del vehiculo
        /// </summary>
        public int VehicleId { get; set; }
        
        /// <summary>
        /// Latitud en la que se encuentra el vehículo
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Longitud en la que se encuentra el vehículo
        /// </summary>        
        public double Longitude { get; set; }
    }
}
