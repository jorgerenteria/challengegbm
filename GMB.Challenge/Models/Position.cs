﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GMB.Challenge.Models
{
    /// <summary>
    /// Modelo que contiene la posición del vehiculo
    /// </summary>
    public class Position
    {
        /// <summary>
        /// Latitud en la que se encuentra el vehículo
        /// </summary>
        [Required]
        public double? Latitude { get; set; }

        /// <summary>
        /// Longitud en la que se encuentra el vehículo
        /// </summary>        
        [Required]        
        public double? Longitude { get; set; }
    }
}
