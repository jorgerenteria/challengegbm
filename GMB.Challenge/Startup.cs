﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GBM.Component.Contract;
using GBM.Component.Implementation;
using GMB.Challenge.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Swashbuckle.AspNetCore.Swagger;

namespace GMB.Challenge
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "GBM Challenge",
                    Description = "Web API to manage vehicles in real time",
                    TermsOfService = "None",
                    Contact = new Contact() { Name = "Jorge Renteria", Email = "jorgerenteria222@gmail.com", Url = "nothing" }
                });
                c.IncludeXmlComments(GetXmlCommentsPath());
                c.EnableAnnotations();
            });
            Mapper.Initialize(cfg => {
                cfg.CreateMap<VehiclePositionDb, VehiclePositionDto>();
                cfg.CreateMap<VehiclePositionDto, VehiclePositionDb>();
                cfg.CreateMap<VehiclePosition, VehiclePositionDto>();
                cfg.CreateMap<VehiclePositionDto, VehiclePosition>();
            });
            services.AddSingleton<IMongoDatabase>(dataBase => {
                var client = new MongoClient("mongodb://localhost:27017");
                var db = client.GetDatabase("GBMChallenge");
                return db;
            });
            services.AddScoped<IVehiclePositionRepository, VehiclePositionRepository>();
            services.AddAutoMapper();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "GBM Challenge");
            });
        }

        private string GetXmlCommentsPath()
        {
            var app = System.AppContext.BaseDirectory;
            return System.IO.Path.Combine(app, "GBM.Challenge.xml");
        }
    }
}
